﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class Rotating : MonoBehaviour
{

    private Transform leftController;
    private Transform rightController;
    private VRTK_SnapDropZone snapZone;
    private bool mieli = false;

    private void OnEnable()
    {
        snapZone = GetComponent<VRTK_SnapDropZone>();
        snapZone.ObjectEnteredSnapDropZone += OnObjectEnteredSnapDropZone;
        snapZone.ObjectExitedSnapDropZone += OnObjectExitedSnapDropZone;
        snapZone.ObjectSnappedToDropZone += OnObjectSnappedToDropZone;
    }

    private void OnObjectSnappedToDropZone(object sender, SnapDropZoneEventArgs e)
    {
        mieli = false;
    }
    private void OnObjectExitedSnapDropZone(object sender, SnapDropZoneEventArgs e)
    {
        mieli = false;
    }
    private void OnObjectEnteredSnapDropZone(object sender, SnapDropZoneEventArgs e)
    {
        mieli = true;
    }

    private void Update()
    {
        if(leftController == null || rightController == null)
        {
            leftController = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.LeftController);
            rightController = VRTK_DeviceFinder.DeviceTransform(VRTK_DeviceFinder.Devices.RightController);
        }
        if (mieli)
        {
            SprawdzaSeiUstawia();
        }
    }
    private void SprawdzaSeiUstawia()
    {

        var leftHandGrabbedObject = leftController.gameObject.GetComponent<VRTK_InteractGrab>().GetGrabbedObject();
        var rightHandGrabbedObject = rightController.gameObject.GetComponent<VRTK_InteractGrab>().GetGrabbedObject();

        if (leftHandGrabbedObject != null)
        {
            // Dorobic przedziały, bo jest chujnia
            snapZone.transform.localRotation = Quaternion.Euler(0, 0, leftHandGrabbedObject.transform.parent.localRotation.z);
        }
        if (rightHandGrabbedObject != null)
        {
            snapZone.transform.localRotation = Quaternion.Euler(0, 0, rightHandGrabbedObject.transform.parent.localRotation.z);
        }  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rotating>() != null)
            snapZone.ForceUnsnap();
    }

}
